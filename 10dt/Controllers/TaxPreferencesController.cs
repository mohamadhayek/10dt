﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using _10dt.Models;

namespace _10dt.Controllers
{
    public class TaxPreferencesController : Controller
    {
        private _10dtContext db = new _10dtContext();

        // GET: TaxPreferences
        public ActionResult Index()
        {
            return View(db.TaxPreferences.ToList());
        }

        // GET: TaxPreferences/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TaxPreference taxPreference = db.TaxPreferences.Find(id);
            if (taxPreference == null)
            {
                return HttpNotFound();
            }
            return View(taxPreference);
        }

        // GET: TaxPreferences/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TaxPreferences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "taxPreferenceID,tenDTLevel,payment,user")] TaxPreference taxPreference)
        {
            if (ModelState.IsValid)
            {
                
                db.TaxPreferences.Add(taxPreference);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(taxPreference);
        }

        // GET: TaxPreferences/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TaxPreference taxPreference = db.TaxPreferences.Find(id);
            if (taxPreference == null)
            {
                return HttpNotFound();
            }
            return View(taxPreference);
        }

        // POST: TaxPreferences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "taxPreferenceID,tenDTLevel,payment")] TaxPreference taxPreference)
        {
            if (ModelState.IsValid)
            {
                db.Entry(taxPreference).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(taxPreference);
        }

        // GET: TaxPreferences/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TaxPreference taxPreference = db.TaxPreferences.Find(id);
            if (taxPreference == null)
            {
                return HttpNotFound();
            }
            return View(taxPreference);
        }

        // POST: TaxPreferences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TaxPreference taxPreference = db.TaxPreferences.Find(id);
            db.TaxPreferences.Remove(taxPreference);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
