﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _10dt.Models
{
    public class User
    {
        
        public int ID { get; set; }
        public int primaryAccountStatus { get; set; }

        [Required]
        public string userName { get; set; }
        //[Required]
        [StringLength(16, MinimumLength = 8, ErrorMessage = "Password must be between 8 and 16 characters long, and include letters, numbers, and symbols.")]
        [RegularExpression(@"^.*(?=.*[!@#$%^&*\(\)_\-+=]).*$",
        ErrorMessage = "Please include numbers, letters, and symbols in your password")]
        [DataType(DataType.Password)]
        public string password { get; set; }
        public string title { get; set; }

        [Required(ErrorMessage = "Please enter a first name")]
        [StringLength(20)]
        public string firstName { get; set; }
        [Required(ErrorMessage = "Please enter a first name")]
        [StringLength(50)]
        public string lastName { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string provinceState { get; set; }
        public string country { get; set; }
        public string postalCode { get; set; }

        [Required]
        [Phone(ErrorMessage = "Please enter a valid phone number.")]
        public string phone { get; set; }
        public string socialInsuranceNum { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Please enter a valid email.")]
        public string email { get; set; }
        public int marriage { get; set; }
        public string industry { get; set; }
        public DateTime? maritalStatusChange { get; set; }
        public string citizenshipStatus { get; set; }
        public int spouseID { get; set; }
        public Boolean authorizeCRAInfo { get; set; }
        public string specialDetails { get; set; }
        public int activityStatus { get; set; }
        public DateTime? dateOfBirth { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public DateTime? deletedAt { get; set; }
        public DateTime? blacklistedAt { get; set; }
        public Boolean blacklistedStatus { get; set; }

        public virtual ICollection<TaxPreference> TaxPreferences { get; set; }
    }


}