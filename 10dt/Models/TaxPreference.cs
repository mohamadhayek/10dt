﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _10dt.Models
{
    public class TaxPreference
    {
        public int taxPreferenceID { get; set; }
        public string tenDTLevel { get; set; }
        public Boolean payment { get; set; }
        public int UserID { get; set; }
        
        public virtual User User { get; set; }
        
    }

    public enum tenDTLevel
    {
        Basic,
        Moderate,
        Complex
    }
}