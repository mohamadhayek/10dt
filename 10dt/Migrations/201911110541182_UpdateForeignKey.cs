﻿namespace _10dt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateForeignKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TaxPreferences", "User_ID", "dbo.Users");
            DropIndex("dbo.TaxPreferences", new[] { "User_ID" });
            RenameColumn(table: "dbo.TaxPreferences", name: "User_ID", newName: "UserID");
            AlterColumn("dbo.TaxPreferences", "UserID", c => c.Int(nullable: false));
            CreateIndex("dbo.TaxPreferences", "UserID");
            AddForeignKey("dbo.TaxPreferences", "UserID", "dbo.Users", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaxPreferences", "UserID", "dbo.Users");
            DropIndex("dbo.TaxPreferences", new[] { "UserID" });
            AlterColumn("dbo.TaxPreferences", "UserID", c => c.Int());
            RenameColumn(table: "dbo.TaxPreferences", name: "UserID", newName: "User_ID");
            CreateIndex("dbo.TaxPreferences", "User_ID");
            AddForeignKey("dbo.TaxPreferences", "User_ID", "dbo.Users", "ID");
        }
    }
}
