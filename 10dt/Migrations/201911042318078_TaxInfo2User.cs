﻿namespace _10dt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TaxInfo2User : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "tenDTLevel", c => c.String());
            AddColumn("dbo.Users", "payment", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "payment");
            DropColumn("dbo.Users", "tenDTLevel");
        }
    }
}
