﻿namespace _10dt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDatetime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "maritalStatusChange", c => c.DateTime());
            AlterColumn("dbo.Users", "dateOfBirth", c => c.DateTime());
            AlterColumn("dbo.Users", "createdAt", c => c.DateTime());
            AlterColumn("dbo.Users", "updatedAt", c => c.DateTime());
            AlterColumn("dbo.Users", "deletedAt", c => c.DateTime());
            AlterColumn("dbo.Users", "blacklistedAt", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "blacklistedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "deletedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "updatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "createdAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "dateOfBirth", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "maritalStatusChange", c => c.DateTime(nullable: false));
        }
    }
}
