﻿namespace _10dt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TaxPreferences",
                c => new
                    {
                        taxPreferenceID = c.Int(nullable: false, identity: true),
                        tenDTLevel = c.String(),
                        payment = c.Boolean(nullable: false),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.taxPreferenceID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.User_ID);
            
            AlterColumn("dbo.Users", "password", c => c.String(maxLength: 16));
            DropColumn("dbo.Users", "tenDTLevel");
            DropColumn("dbo.Users", "payment");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "payment", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "tenDTLevel", c => c.String());
            DropForeignKey("dbo.TaxPreferences", "User_ID", "dbo.Users");
            DropIndex("dbo.TaxPreferences", new[] { "User_ID" });
            AlterColumn("dbo.Users", "password", c => c.String(nullable: false, maxLength: 16));
            DropTable("dbo.TaxPreferences");
        }
    }
}
