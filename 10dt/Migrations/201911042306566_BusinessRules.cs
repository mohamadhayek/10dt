﻿namespace _10dt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BusinessRules : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "userName", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "password", c => c.String(nullable: false, maxLength: 16));
            AlterColumn("dbo.Users", "firstName", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Users", "lastName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Users", "phone", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "email", c => c.String());
            AlterColumn("dbo.Users", "phone", c => c.String());
            AlterColumn("dbo.Users", "lastName", c => c.String());
            AlterColumn("dbo.Users", "firstName", c => c.String());
            AlterColumn("dbo.Users", "password", c => c.String());
            AlterColumn("dbo.Users", "userName", c => c.String());
        }
    }
}
